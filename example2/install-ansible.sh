#!/bin/sh

export DEBIAN_FRONTEND=noninteractive

apt update
apt install software-properties-common
add-apt-repository --yes --update ppa:ansible/ansible
apt install -y ansible
